# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.14.0
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# # Split the Bookchapter tutorial in single files
#
# Writes to directory <infile>-parts
# Single files are numbered, where we can set symbolic names in the Notebook, using lines
# ```
#     #[name]
# ```

# +
import re
import json
import os

infile = "Bookchapter Tutorial.ipynb"
base = re.sub('.ipynb$','',infile)

outdir = f"{base}-parts"
try:
    os.mkdir(outdir)
except:
    pass

# +
with open(infile) as fh:
    content = json.load(fh)

counter = 0
name = ""

for cell in content['cells']:
    if cell['cell_type'] != 'code': continue
    lines = list()
    for line in cell['source']:
        m = re.match(r'^#\[(.*)\]',line)
        if m:
            name = f"{m[1]}-"
            counter = 0
        else:
            lines.append(line)
            
    outfile = f"{outdir}/{name}{counter:02}" 
    print(outfile)
    with open(outfile,'w') as fh:
        for line in lines:
            fh.write(line)
        fh.close()
    counter += 1
# -




